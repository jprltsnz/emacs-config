;; -*- lexical-binding: t -*-


;;----------------------------------------------------------------------------
;; Adjust garbage collection thresholds during startup, and thereafter
;;----------------------------------------------------------------------------
(let ((normal-gc-cons-threshold (* 20 1024 1024))
      (init-gc-cons-threshold (* 128 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'emacs-startup-hook
            (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

(defvar my-emacs-dir
  (eval-when-compile (file-truename user-emacs-directory))
  "The path to this emacs.d directory. Must end in a slash.")

(defvar my-modules-dir (concat my-emacs-dir "modules/")
  "The root directory of modules files.")

(defvar my-local-dir (concat my-emacs-dir ".local/")
  "Root directory for local Emacs files. Use this as permanent storage for files
that are safe to share across systems (if this config is symlinked across
several computers).")

(defvar no-littering-etc-directory (concat my-local-dir "etc/")
  "Directory for non-volatile storage.
Use this for files that don't change much, like servers binaries, external
dependencies or long-term shared data.")

(defvar no-littering-var-directory (concat my-local-dir "cache/")
  "Directory for volatile storage.
Use this for files that change often, like cache files.")

(let ((default-directory my-modules-dir))
  (normal-top-level-add-to-load-path '("."))
  (normal-top-level-add-subdirs-to-load-path))
;; Computer specific setting

(if (file-exists-p (concat my-emacs-dir "computer-config.el"))
(load-file (concat my-emacs-dir "computer-config.el")))
(load-file (concat my-emacs-dir "bootstrap.el"))

(use-package exec-path-from-shell
  :demand t
  :config (exec-path-from-shell-initialize))

(require 'core)
(require 'evil-config)
(require 'ui)
(require 'winbuf-management)
(require 'ivy-config)
(require 'company-config)
(require 'flycheck-config)
(require 'spellcheck)
(require 'vc-magit)
(require 'parens-config)
(require 'ranger-config)
(require 'org-config)
(require 'treemacs-config)
(require 'term-config)
(require 'pdf-config)
(require 'python-config)
(require 'sh-config)
(require 'cc-config)
(require 'rust-config)
(require 'latex-config)
(require 'typescript-config)
(require 'restclient-config)
(require 'yasnippets-config)
(require 'data-config)
(require 'keybindings)
