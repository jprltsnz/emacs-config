
(defvar +org-babel-mode-alist
  '((cpp . C)
    (C++ . C)
    (D . C)
    (sh . shell)
    (bash . shell)
    (matlab . octave)))

(use-package org-bullets
  :hook (org-mode . org-bullets-mode))

(use-package toc-org
  :hook (org-mode . toc-org-mode))

(use-package evil-org
  :hook (org-mode . evil-org-mode))

(use-package ox-pandoc)

  (use-package org-download
    :commands (org-download-enable
               org-download-yank
               org-download-screenshot))

(use-package org-plus-contrib)

(use-package org
  :commands (org-mode)
  :init
  (setq org-directory "~/Documents/org/"
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-log-done t
        org-startup-with-inline-images t
        org-image-actual-width nil
        org-src-fontify-natively t
        org-src-tab-acts-natively t
        org-imenu-depth 8
        org-src-fontify-natively t
        org-src-preserve-indentation t
        org-src-tab-acts-natively t
        org-src-window-setup 'current-window
        org-confirm-babel-evaluate nil)
  :config
  (defun +org*babel-lazy-load-library (info)
    "Load babel libraries as needed when babel blocks are executed."
    (let* ((lang (nth 0 info))
           (lang (if (symbolp lang) lang (intern lang)))
           (lang (or (cdr (assq lang +org-babel-mode-alist))
                     lang)))
      (when (and (not (cdr (assq lang org-babel-load-languages)))
                 (or (run-hook-with-args-until-success '+org-babel-load-functions lang)
                     (require (intern (format "ob-%s" lang)) nil t)))
        (when (assq :async (nth 2 info))
          ;; ob-async has its own agenda for lazy loading packages (in the
          ;; child process), so we only need to make sure it's loaded.
          (require 'ob-async nil t))
        (add-to-list 'org-babel-load-languages (cons lang t)))
      t))
  (advice-add #'org-babel-confirm-evaluate :after-while #'+org*babel-lazy-load-library))

(use-package org-noter
  :commands (org-noter)
  :config
  (my-local-leader-def
    :keymaps 'pdf-view-mode-map
    :states 'normal
    "i" #'org-noter-insert-note
    "I" #'org-noter-insert-precise-note
    "q" #'org-noter-kill-session
    "n" #'org-noter-sync-next-page-or-chapter
    "N" #'org-noter-sync-next-note
    "p" #'org-noter-sync-prev-page-or-chapter
    "P" #'org-noter-sync-prev-note
    "c" #'org-noter-sync-current-page-or-chapter
    "C" #'org-noter-sync-current-note))

(provide 'org-config)
