;;; -*- lexical-binding: t; -*-
(require 'various-func)

(general-define-key
 "M-s" #'save-buffer
 "C-u" #'evil-scroll-up)

(my-leader-def
  :states '(normal motion)
  "u" #'universal-argument
  ; help
  "h" '(:ignore t :wk "Help")
  "hf" #'describe-function
  "hF" #'describe-face
  "hk" #'describe-key
  "hm" #'describe-mode
  "hv" #'describe-variable
  ;; file commands
  "f" '(:ignore t :wk "Files")
  "ff" '(find-file :which-key "Browse files")
  "fr" '(recentf-open-files :which-key "Recent files")
  "f<" #'doom/sudo-find-file
  "fd" #'dired
  "ft" #'neotree-toggle
  ;; Window commands
  "w" '(:ignore t :wk "Windows")
  "wv" #'evil-window-vsplit
  "ws" #'evil-window-split
  "ww" #'evil-window-next
  "w]" #'evil-window-next
  "w[" #'evil-window-prev
  "wj" #'evil-window-down
  "wk" #'evil-window-up
  "wl" #'evil-window-right
  "wh" #'evil-window-left
  "wd" #'evil-window-delete
  "wJ" #'+evil/window-move-down
  "wK" #'+evil/window-move-up
  "wH" #'+evil/window-move-left
  "wL" #'+evil/window-move-right
  "wD" '((lambda ()(interactive)(kill-current-buffer)(evil-window-delete)) :wk "delete window and buffer")
  ;; Buffer
  "b" '(:ignore t :wk "Buffers")
  "bb" #'switch-to-buffer
  "b<" #'doom/sudo-this-file
  "bB" #'ibuffer
  "b]" #'next-buffer
  "b[" #'previous-buffer
  "bd" #'kill-current-buffer
  "bD" '((lambda ()(interactive)(kill-current-buffer)(evil-window-delete)) :wk "delete window and buffer")
  ;; Workspaces
  "<tab>" '(:ignore :wk "Workspaces")
  "<tab>n" #'eyebrowse-create-window-config
  "<tab>r" #'eyebrowse-rename-window-config
  "<tab>d" #'eyebrowse-close-window-config
  "<tab>]" #'eyebrowse-next-window-config
  "<tab>[" #'eyebrowse-prev-window-config
  "<tab><tab>" #'eyebrowse-switch-to-window-config
  "<tab>0" #'eyebrowse-switch-to-window-config-0
  "<tab>1" #'eyebrowse-switch-to-window-config-1
  "<tab>2" #'eyebrowse-switch-to-window-config-2
  "<tab>3" #'eyebrowse-switch-to-window-config-3
  "<tab>4" #'eyebrowse-switch-to-window-config-4
  "<tab>5" #'eyebrowse-switch-to-window-config-5
  "<tab>6" #'eyebrowse-switch-to-window-config-6
  "<tab>7" #'eyebrowse-switch-to-window-config-7
  "<tab>8" #'eyebrowse-switch-to-window-config-8
  "<tab>9" #'eyebrowse-switch-to-window-config-9
  ;; Next
  "]" '(:ignore t :wk "Next")
  "]b" #'next-buffer
  "]w" #'evil-window-next
  "]]" #'text-scale-increase
  "]<tab>" #'eyebrowse-next-window-config
  ;; Previous
  "[" '(:ignore t :wk "Previous")
  "[b" #'previous-buffer
  "[w" #'evil-window-prev
  "[[" #'text-scale-decrease
  "[<tab>" #'eyebrowse-prev-window-config
  ;; Project
  "p" '(:ignore t :wk "Project")
  "pf" #'projectile-find-file
  "p!" #'projectile-run-shell-command-in-root
  "pc" #'projectile-compile-project
  "po" #'projectile-find-other-file
  "pp" #'projectile-switch-project
  "pr" #'projectile-recentf
  "px" #'projectile-invalidate-cache
  "pt" #'+neotree/toggle
  "pT" #'+neotree/find-this-file
   ;; Magit
  "g" '(:ignore t :wk "Git")
  "gg" #'magit-status
  "gb" #'magit-blame
  "gt" #'git-timemachine
  )

(provide 'keybindings)
