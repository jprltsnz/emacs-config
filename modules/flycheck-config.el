(use-package flycheck
  :commands (flycheck-list-errors flycheck-buffer)
  :config
  (setq flycheck-check-syntax-automatically '(save idle-change mode-enabled))
  :hook (after-init . global-flycheck-mode))
(provide 'flycheck-config)
