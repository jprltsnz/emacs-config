
;;;###autoload
(defun fd-switch-dictionary()
  (interactive)
  (let* ((dic ispell-current-dictionary)
         (change (if (string= dic "english") "espanol" "english")))
    (ispell-change-dictionary change)
    (message "Dictionary switched from %s to %s" dic change)
    (flyspell-buffer)))

(use-package flyspell
  :init
  (setq ispell-dictionary "english"
      ispell-list-command "--list"
      ispell-extr-args '("--dont-tex-check-comments" "--sug-mode=ultra")
      flyspell-issue-message-flag nil)
  (add-hook 'flyspell-mode-hook #'flyspell-buffer)
  :commands (flyspell-buffer flyspell-mode flyspell-check-previous-highlighted-word ispell-word)
  :hook
  (text-mode . flyspell-mode)
  (prog-mode . flyspell-prog-mode))


(provide 'spellcheck)
