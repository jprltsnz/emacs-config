;; -*- lexical-binding: t -*-
;; Evil
(use-package evil
  :demand t
  :init
  (setq-default evil-want-visual-char-semi-exclusive t
                evil-want-integration t
                evil-want-keybinding nil
                evil-magic t
                evil-echo-state t
                evil-indent-convert-tabs t
                evil-ex-search-vim-style-regexp t
                evil-ex-visual-char-range t  ; column range for ex commands
                evil-insert-skip-empty-lines t
                evil-mode-line-format 'nil
                evil-respect-visual-line-mode t
                ;; more vim-like behavior
                evil-symbol-word-search t
                ;; don't activate mark on shift-click
                shift-select-mode nil
                ;; cursor appearance
                evil-default-cursor '+evil-default-cursor
                evil-normal-state-cursor 'box
                evil-emacs-state-cursor  '(box +evil-emacs-cursor)
                evil-insert-state-cursor 'bar
                evil-visual-state-cursor 'hollow)
  :config
  (evil-mode t)
  (general-define-key
   :states '(normal visual motion)
   "SPC" nil)
  (general-create-definer my-leader-def
    ;; :prefix my-leader
    :prefix "SPC")
  (general-create-definer my-local-leader-def
    ;; :prefix my-local-leader
    :prefix "SPC m"))

(use-package evil-collection
  :after evil
  :init
  (setq evil-collection-key-blacklist '("SPC"))
  :hook (after-init . evil-collection-init))

(use-package evil-commentary
  :hook
  (prog-mode . evil-commentary-mode)
  (TeX-mode . evil-commentary-mode)
  :general
  (:keymaps'evil-commentary-mode-map
   :states 'visual
   "gc" #'evil-commentary
   ))

(use-package vi-tilde-fringe
  :hook
  (prog-mode . vi-tilde-fringe-mode)
  (text-mode . vi-tilde-fringe-mode)
  (conf-mode . vi-tilde-fringe-mode))

(provide 'evil-config)
