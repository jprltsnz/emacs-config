;;; -*- lexical-binding: t; -*-
(use-package company
  ;:commands (company-complete-common company-manual-begin company-grab-line)
  :demand t
  :init
    (setq company-idle-delay 0.1
        company-tooltip-limit 14
        company-dabbrev-downcase nil
        company-dabbrev-ignore-case nil
        company-dabbrev-code-other-buffers t
        company-tooltip-align-annotations t
        company-require-match 'never
        company-global-modes
        '(not erc-mode message-mode help-mode gud-mode eshell-mode)
        ;company-backends '(company-capf)
        company-frontends
        '(company-pseudo-tooltip-frontend
         company-echo-metadata-frontend))
    :general
   ([remap completion-at-point] #'company-manual-begin
    [remap complete-symbol]  #'company-manual-begin)
    :hook (after-init . global-company-mode))

(use-package company-tng
 :demand t
 :straight company
 :config
  (add-to-list 'company-frontends 'company-tng-frontend)
 :general
 (:keymaps 'company-active-map
            "RET"       nil
            [return]    nil
           "TAB"       #'company-select-next
           [tab]       #'company-select-next
           "<backtab>" #'company-select-previous
           [backtab] #'company-select-previous))

(use-package company-prescient
  :hook (company-mode . company-prescient-mode)
  :config
  (prescient-persist-mode +1))

(use-package company-quickhelp
  :hook (company-mode . company-quickhelp-mode))

(use-package company-dict
  :demand t
  :config
  (setq company-dict-dir (concat no-littering-var-directory "dict/"))
  (add-to-list 'company-backends 'company-dict))

(provide 'company-config)
