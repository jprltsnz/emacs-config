;; -*- lexical-binding: t -*-


(require 'cl)
(defun +evil--window-swap (direction)
  "Move current window to the next window in DIRECTION.
If there are no windows there and there is only one window, split in that
direction and place this window there. If there are no windows and this isn't
the only window, use evil-window-move-* (e.g. `evil-window-move-far-left')."
  (when (window-dedicated-p)
    (user-error "Cannot swap a dedicated window"))
  (let* ((this-window (selected-window))
         (this-buffer (current-buffer))
         (that-window (windmove-find-other-window direction nil this-window))
         (that-buffer (window-buffer that-window)))
    (when (or (minibufferp that-buffer)
              (window-dedicated-p this-window))
      (setq that-buffer nil that-window nil))
    (if (not (or that-window (one-window-p t)))
        (funcall (pcase direction
                   ('left  #'evil-window-move-far-left)
                   ('right #'evil-window-move-far-right)
                   ('up    #'evil-window-move-very-top)
                   ('down  #'evil-window-move-very-bottom)))
      (unless that-window
        (setq that-window
              (split-window this-window nil
                            (pcase direction
                              ('up 'above)
                              ('down 'below)
                              (_ direction))))
        (with-selected-window that-window
          (switch-to-buffer ("*scratch*")))
        (setq that-buffer (window-buffer that-window)))
      (with-selected-window this-window
        (switch-to-buffer that-buffer))
      (with-selected-window that-window
        (switch-to-buffer this-buffer))
      (select-window that-window))))

;;;###autoload
(evil-define-command +evil*window-split (&optional count file)
  "Same as `evil-window-split', but focuses (and recenters) the new split."
  :repeat nil
  (interactive "P<f>")
  (split-window (selected-window) count
                (if evil-split-window-below 'above 'below))
  (call-interactively
   (if evil-split-window-below
       #'evil-window-up
     #'evil-window-down))
  (recenter)
  (when (and (not count) evil-auto-balance-windows)
    (balance-windows (window-parent)))
  (if file (evil-edit file)))

;;;###autoload
(evil-define-command +evil*window-vsplit (&optional count file)
  "Same as `evil-window-vsplit', but focuses (and recenters) the new split."
  :repeat nil
  (interactive "P<f>")
  (split-window (selected-window) count
                (if evil-vsplit-window-right 'left 'right))
  (call-interactively
   (if evil-vsplit-window-right
       #'evil-window-left
     #'evil-window-right))
  (recenter)
  (when (and (not count) evil-auto-balance-windows)
    (balance-windows (window-parent)))
(if file (evil-edit file)))

;;;###autoload
(defun +evil/window-move-left () "See `+evil--window-swap'"  (interactive) (+evil--window-swap 'left))
;;;###autoload
(defun +evil/window-move-right () "See `+evil--window-swap'" (interactive) (+evil--window-swap 'right))
;;;###autoload
(defun +evil/window-move-up () "See `+evil--window-swap'"    (interactive) (+evil--window-swap 'up))
;;;###autoload
(defun +evil/window-move-down () "See `+evil--window-swap'"  (interactive) (+evil--window-swap 'down))


(advice-add #'evil-window-split  :override #'+evil*window-split)
(advice-add #'evil-window-vsplit :override #'+evil*window-vsplit)


(use-package eyebrowse
  :init
  (setq eyebrowse-mode-line-separator " | "
        eyebrowse-wrap-around t
        eyebrowse-default-workspace-slot 1
        eyebrowse-mode-line-style t
        eyebrowse-new-workspace t)
  :config
  (general-define-key
   :states 'motion
   "gt" #'eyebrowse-next-window-config
   "gT" #'eyebrowse-prev-window-config
   "gc" #'eyebrowse-close-window-config
   "zx" #'eyebrowse-last-window-config)
  :hook (after-init . eyebrowse-mode))

(provide 'winbuf-management)
