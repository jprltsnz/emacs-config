;;; modules/yasnippets-config.el -*- lexical-binding: t; -*-

(use-package yasnippet
  :commands (yas-minor-mode-on yas-expand yas-expand-snippet yas-lookup-snippet
                                yas-insert-snippet yas-new-snippet yas-visit-snippet-file)

  :config
  (setq yas-also-auto-indent-first-line t
        yas-triggers-in-field t) ; Allow nested snippets
  :hook (after-init . yas-global-mode))

(use-package yasnippet-snippets)

(provide 'yasnippets-config)
