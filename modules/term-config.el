
(use-package sane-term
  :commands (sane-term sane-term-create))

(provide 'term-config)
