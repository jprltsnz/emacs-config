
;; Magit related
(use-package magit
  :defer t
  :commands (magit-status)
  :config (require 'evil-magit))

(use-package evil-magit
  :defer t)

;; Others
(use-package git-timemachine
 :commands (git-timemachine-toggle)
 :config
 (add-hook 'git-timemachine-mode-hook #'evil-normalize-keymaps))
(use-package git-gutter-fringe
  :commands (git-gutter:revert-hunk git-gutter:stage-hunk)
  :hook
  (prog-mode . git-gutter-mode)
  (text-mode . git-gutter-mode)
  (conf-mode . git-gutter-mode))
(provide 'vc-magit)
