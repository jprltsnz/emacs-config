;;; -*- lexical-binding: t; -*-



(use-package pdf-tools
  :init
  (add-hook 'pdf-view-mode-hook   (lambda () (set (make-local-variable 'evil-emacs-state-cursor) (list nil))))
  :config
  (pdf-loader-install)
  (my-local-leader-def
    :keymaps 'pdf-view-mode-map
    :states '(normal visual)
    "aa" #'pdf-annot-add-highlight-markup-annotation
    "ad" #'pdf-annot-delete
    "al" #'pdf-annot-list-annotations)
 (defun +pdf|cleanup-windows ()
    "Kill left-over annotation buffers when the document is killed."
    (when (buffer-live-p pdf-annot-list-document-buffer)
      (pdf-info-close pdf-annot-list-document-buffer))
    (when (buffer-live-p pdf-annot-list-buffer)
      (kill-buffer pdf-annot-list-buffer))
    (let ((contents-buffer (get-buffer "*Contents*")))
      (when (and contents-buffer (buffer-live-p contents-buffer))
        (kill-buffer contents-buffer))))
  (add-hook 'pdf-view-mode-hook (lambda () (add-hook 'kill-buffer-hook #'+pdf|cleanup-windows nil t)))
  :hook (pdf-view-mode . pdf-annot-minor-mode)
  :mode ("\\.pdf\\'" . pdf-view-mode))

(provide 'pdf-config)
