;;; ranger-config.el -*- lexical-binding: t; -*-

(use-package ranger
  :init
  (setq ranger-enter-with-minus t
        ranger-cleanup-eagerly t
        ranger-show-hidden nil
        ranger-parent-depth 1
        ranger-ignored-extensions '("mkv" "iso" "mp4")
        ranger-max-preview-size 10)
  :general
  ([remap dired] #'ranger)
  )


(provide 'ranger-config)
