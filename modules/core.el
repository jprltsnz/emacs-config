; key-binding setup
(use-package general
  :demand t)

(use-package which-key
  :demand t
  :config
  (setq which-key-sort-order #'which-key-prefix-then-key-order
        which-key-sort-uppercase-first nil
        which-key-add-column-padding 1
        which-key-max-display-columns nil
        which-key-min-display-lines 5)
  ;; embolden local bindings
  (set-face-attribute 'which-key-local-map-description-face nil :weight 'bold)
  (which-key-setup-side-window-bottom)
(add-hook 'after-init-hook #'which-key-mode))

(use-package hydra
  :demand t
  :init
  (setq lv-use-seperator t))



; Emacs core configuration

;; UTF-8 as the default coding system
(when (fboundp 'set-charset-priority)
  (set-charset-priority 'unicode))     ; pretty
(prefer-coding-system        'utf-8)   ; pretty
(set-terminal-coding-system  'utf-8)   ; pretty
(set-keyboard-coding-system  'utf-8)   ; pretty
(set-selection-coding-system 'utf-8)   ; perdy
(setq locale-coding-system   'utf-8)   ; please
(setq-default buffer-file-coding-system 'utf-8) ; with sugar on top

(defalias 'yes-or-no-p 'y-or-n-p)

(menu-bar-mode -1)
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))

(show-paren-mode 1)

(setq-default
 large-file-warning-threshold 30000000
;; Save clipboard contents into kill-ring before replacing them
 save-interprogram-paste-before-kill t
 bookmark-save-flag t
; Scrolling
 hscroll-margin 0
 hscroll-step 2
 scroll-conservatively 1001
 scroll-margin 0
 scroll-preserve-screen-position t

 version-control t
 vc-follow-symlinks t
 delete-old-versions -1
 ring-bell-function 'ignore
 ; Formating
 delete-trailing-lines nil
 fill-column 80
 sentence-end-double-space nil
 word-wrap t
 ;; Whitespace (see `editorconfig')
 indent-tabs-mode nil
 require-final-newline t
 tab-always-indent t
 tab-width 4
 ;; Wrapping
 truncate-lines t
truncate-partial-width-windows 50

 ad-redefinition-action 'accept   ; silence advised function warnings
 apropos-do-all t                 ; make `apropos' more useful
 auto-mode-case-fold nil
 autoload-compute-prefixes nil
 ffap-machine-p-known 'reject     ; don't ping things that look like domain names
 idle-update-delay 2              ; update ui less often
 ;; be quiet at startup; don't load or display anything unnecessary
 inhibit-startup-message t
 inhibit-startup-echo-area-message user-login-name
 inhibit-default-init t
 initial-major-mode 'fundamental-mode
 initial-scratch-message nil
 ;; keep the point out of the minibuffer
 minibuffer-prompt-properties '(read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt)
 ;; History & backup settings (save nothing, that's what git is for)
 auto-save-default nil
 create-lockfiles nil
 history-length 500
 make-backup-files nil  ; don't create backup~ files
 ;; byte compilation
 byte-compile-warnings '(not free-vars unresolved noruntime lexical make-local)
 ;; security
 gnutls-verify-error (not (getenv "INSECURE")) ; you shouldn't use this
 tls-checktrust gnutls-verify-error
 tls-program (list "gnutls-cli --x509cafile %t -p %p %h"
                   ;; compatibility fallbacks
                   "gnutls-cli -p %p %h"
                   "openssl s_client -connect %h:%p -no_ssl2 -no_ssl3 -ign_eof"))

(setq prescient-save-file (concat no-littering-var-directory "prescient-save.el"))
;; Remove hscroll-margin in shells, otherwise it causes jumpiness
(add-hook 'term-mode-hook #'(lambda () (setq hscroll-margin 0)))

;;; Configure ~/.emacs.d paths

;; Package `no-littering' changes the default paths for lots of
;; different packages, with the net result that the ~/.emacs.d folder
;; is much more clean and organized.
(use-package no-littering
  :demand t)

(use-package recentf
  :demand t
  :commands recentf-open-files
  :config
  (setq recentf-auto-cleanup 'mode
        recentf-max-menu-items 0
        recentf-max-saved-items 300
        recentf-filename-handlers '(file-truename abbreviate-file-name)
        recentf-exclude
        (list #'file-remote-p "\\.\\(?:gz\\|gif\\|svg\\|png\\|jpe?g\\)$"
              "^/tmp/" "^/ssh:" "\\.?ido\\.last$" "\\.revive$" "/TAGS$"
              "^/var/folders/.+$" ".local/etc/workspaces/.+$"))
  :hook
  (kill-emacs . recentf-cleanup)
  (after-init . recentf-mode))

(use-package savehist
  :config
  (setq savehist-autosave-interval nil
        savehist-save-minibuffer-history t
         savehist-aditional-variables '(search-ring regexp-search-ring))
   :hook (after-init . savehist-mode))
(use-package autorevert
  :init
  (setq auto-revert-verbose nil)
  :hook (after-init . global-auto-revert-mode))
(use-package saveplace
  :hook (after-init . save-place-mode))

(use-package projectile
  :demand t
  :commands (projectile-project-root projectile-project-name projectile-project-p)
  :init
  (setq projectile-require-project-root t
        projectile-globally-ignored-files '(".DS_Store" "Icon" "TAGS")
        projectile-globally-ignored-file-suffixes '(".elc" ".pyc" ".o")
        projectile-ignored-projects '("~/" "/tmp"))

  :config
  (add-hook 'dired-before-readin-hook #'projectile-track-known-projects-find-file-hook)
  (projectile-mode +1)

  ;; a more generic project root file
  (push ".project" projectile-project-root-files-bottom-up)

  (setq projectile-globally-ignored-directories
        (append projectile-globally-ignored-directories
                (list ".sync" "node_modules" "flow-typed"))
        projectile-other-file-alist
        (append projectile-other-file-alist
                '(("css"  "scss" "sass" "less" "styl")
                  ("scss" "css")
                  ("sass" "css")
                  ("less" "css")
                  ("styl" "css"))))

  ;; It breaks projectile's project root resolution if HOME is a project (e.g.
  ;; it's a git repo). In that case, we disable bottom-up root searching to
  ;; prevent issues. This makes project resolution a little slower and less
  ;; accurate in some cases.
  (let ((default-directory "~"))
    (when (cl-find-if #'projectile-file-exists-p
                      projectile-project-root-files-bottom-up)
      (message "HOME appears to be a project. Disabling bottom-up root search.")
      (setq projectile-project-root-files
            (append projectile-project-root-files-bottom-up
                    projectile-project-root-files)
            projectile-project-root-files-bottom-up nil)))
  ;; Projectile root-searching functions can cause an infinite loop on TRAMP
  ;; connections, so disable them.
  ;; TODO Is this still necessary?
  (defun doom*projectile-locate-dominating-file (orig-fn file name)
    "Don't traverse the file system if on a remote connection."
    (unless (file-remote-p default-directory)
      (funcall orig-fn file name)))
  (advice-add #'projectile-locate-dominating-file :around #'doom*projectile-locate-dominating-file)

  ;; If fd exists, use it for git and generic projects
  ;; fd is a rust program that is significantly faster. It also respects
  ;; .gitignore. This is recommended in the projectile docs
(when (executable-find "fd")
    (setq projectile-git-command "fd . --type f -0 -H -E .git"
projectile-generic-command projectile-git-command)))

;; super-save
(use-package super-save
  :demand t
  :init
  (setq-default super-save-auto-save-when-idle)
  (setq-default auto-save-default nil)
  (setq-default super-save-remote-files nil)
  :config
  (add-to-list 'super-save-triggers 'evil-window-next)
  :hook (after-init . super-save-mode))

(use-package ws-butler
  :hook (prog-mode . ws-butler-mode))

(use-package helpful
  :general
  (
  [remap describe-function] #'helpful-callable
  [remap describe-command]  #'helpful-command
  [remap describe-variable] #'helpful-variable
  [remap describe-key] #'helpful-key))

(provide 'core)
