;; -*- lexical-binding: t -*-

(use-package rust-mode
  :config
  (setq rust-indent-method-chain t))

;; (use-package flycheck-rust
;;   :hook
;;   (rust-mode . flycheck-rust-setup))

;; (use-package racer
;;   :hook
;;   (rust-mode . racer-mode))

(provide 'rust-config)
