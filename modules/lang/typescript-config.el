;;; modules/typescript-config.el -*- lexical-binding: t; -*-

(use-package typescript-mode
  :config
  (setq-default typescript-indent-level 2))

(use-package tide
  :after (typescript-mode)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)))

(provide 'typescript-config)
