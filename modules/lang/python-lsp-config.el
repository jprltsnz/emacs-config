;; -*- lexical-binding: t -*-

(use-package python-mode
  :straight (:type built-in)
  :commands (python-mode)
  :hook
  (python-mode . (lambda ()
                   (setq-local flycheck-python-pylint-executable "pylint")
                   (setq-local flycheck-python-flake8-executable "flake8")
                   (flycheck-add-next-checker 'lsp-ui 'python-flake8))))

(use-package lsp-python-ms
  :init
  (setq lsp-python-ms-dir (concat no-littering-etc-directory "mspyls/"))
  :hook (python-mode . (lambda ()
                          (require 'lsp-python-ms)
                          (lsp)
                          )))  ; or lsp-deferred

(use-package yapfify
  :commands (yapf-mode yapfify-buffer)
  :init
  (my-local-leader-def
    :keymaps 'python-mode-map
    :states 'normal
    "=" #'yapfify-buffer))

(use-package pyvenv)

(provide 'python-lsp-config)
