;; -*- lexical-binding: t -*-


(use-package auctex)
(add-to-list 'auto-mode-alist '("\\.tex\\'" . TeX-latex-mode))





(provide 'latex-config)
