;; -*- lexical-binding: t -*-

(use-package anaconda-mode
  :init
  (setq anaconda-mode-installation-directory (concat no-littering-etc-directory "anaconda/")
        anaconda-mode-eldoc-as-single-line t)
  (setq anaconda-mode-localhost-address "localhost")
  :config
  (add-hook 'anaconda-mode-hook #'evil-normalize-keymaps)
  (my-local-leader-def
    :keymaps 'anaconda-mode-map
    :states 'normal
    "d" #'anaconda-mode-find-definitions
    "h" #'anaconda-mode-show-doc
    "a" #'anaconda-mode-find-assignments
    "f" #'anaconda-mode-find-file
    "u" #'anaconda-mode-find-references
    "=" #'yapfify-buffer)
  :hook
  (python-mode . anaconda-mode)
  (python-mode . anaconda-eldoc-mode))


(use-package company-anaconda
  :init (push 'company-anaconda company-backends))


(use-package yapfify
  :commands (yapf-mode yapfify-buffer))

(use-package pyvenv)

(provide 'python-config)
