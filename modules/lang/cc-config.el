;; -*- lexical-binding: t -*-

;; (use-package ccls
;;   :init
;;   (add-to-list 'projectile-globally-ignored-directories ".ccls-cache")
;;   (add-to-list 'projectile-project-root-files-bottom-up ".ccls-root")
;;   (add-to-list 'projectile-project-root-files-top-down-recurring "compile_commands.json")
;;   :hook
;;   (c-mode . (lambda ()
;;               (flycheck-add-next-checker 'lsp-ui 'c/c++-clang)
;;               (require 'ccls) (lsp))))

(use-package cmake-mode
  :straight (:host github :repo "emacsmirror/cmake-mode"))

(provide 'cc-config)
