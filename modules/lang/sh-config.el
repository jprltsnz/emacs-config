;;; -*- lexical-binding: t -*-

(use-package company-shell
  :init
  (push 'company-shell company-backends)
  (push 'company-files company-backends)
  :config
  (setq company-shell-delete-duplicates t))


(use-package fish-mode)


(provide 'sh-config)
