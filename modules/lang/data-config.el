;; -*- lexical-binding: t -*-

;; Built in plugins
(add-to-list 'auto-mode-alist '("/sxhkdrc\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.\\(?:hex\\|nes\\)\\'" . hexl-mode))
(add-to-list 'auto-mode-alist '("\\.plist\\'" . nxml-mode))


(use-package toml-mode)
(use-package yaml-mode)
(use-package csv-mode)
(use-package json-mode
  :mode "\\.js\\(?:on\\|[hl]int\\(?:rc\\)?\\)\\'")
(use-package graphql-mode
  :mode "\\.gql\\'")
(use-package dhall-mode)
(use-package protobuf-mode)


(provide 'data-config)
